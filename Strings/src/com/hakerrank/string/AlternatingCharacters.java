package com.hakerrank.string;

import java.util.Scanner;

public class AlternatingCharacters {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int i = Integer.parseInt(sc.nextLine());
		String[] sArr = new String[i];
		int j = 0;
		while (i-- > 0)
			sArr[j++] = sc.nextLine();
		for (String s : sArr)
			calcRepChar(s);
		sc.close();
	}

	public static void calcRepChar(String s) {
		int count = 0;
		int l = s.length();
		for (int i = 0, j = 1; i < l - 1 && j < l; ++i, ++j) {
			if (s.charAt(i) == s.charAt(j))
				count++;
		}
		System.out.println(count);
	}
}
