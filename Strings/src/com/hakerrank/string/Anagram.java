package com.hakerrank.string;

import java.util.Scanner;

public class Anagram {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int l = Integer.parseInt(sc.nextLine());
		String[] sArr = new String[l];
		int i = 0;
		while (l-- > 0)
			sArr[i++] = sc.nextLine();
		for (String s : sArr)
			isAnagram(s);
		sc.close();
	}

	public static void isAnagram(String s) {
		if (s.length() % 2 != 0)
			System.out.println(-1);
		else {
			int[] fArr = new int[26];
			int[] bArr=new int[26];
			int l=s.length();
			for(int i=0;i<l;++i){
				if(i<l/2)
					fArr[s.charAt(i)-'a']++;
				else
					bArr[s.charAt(i)-'a']++;
			}
			int count=0;
			for(int i=0;i<26;++i)
				if(fArr[i]!=0 || bArr[i]!=0) count+=Math.abs(fArr[i]-bArr[i]);
			System.out.println(count/2);
		}
	}

}
