package com.hakerrank.string;

import java.util.Scanner;

public class FunnyString {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int l = Integer.parseInt(scanner.next());
		String[] iArr = new String[l];
		for (int i = 0; i < l; ++i)
			iArr[i] = scanner.next();
		scanner.close();
		for (String s : iArr)
			checkFunny(s);
	}

	public static void checkFunny(String s) {
		int flag = 0;
		int n = s.length();
		for (int i = 0, j = n - 1; i < n - 1 && j > 0; i++, j--) {
			if (Math.abs(s.charAt(i) - s.charAt(i + 1)) != Math.abs(s.charAt(j)
					- s.charAt(j - 1))) {
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			System.out.println("Funny");
		else
			System.out.println("Not funny");
	}

}
