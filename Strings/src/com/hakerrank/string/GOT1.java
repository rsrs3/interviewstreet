package com.hakerrank.string;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GOT1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String in = sc.nextLine();
		isValidKey(in);
		sc.close();
	}

	public static void isValidKey(String s) {
		int[] cArr = new int[26];
		s.chars().forEach(c -> cArr[c - 'a']++);
		Map<Integer, List<Integer>> cMap = IntStream.of(cArr)
				.filter(i -> i > 0).mapToObj(i -> new Integer(i))
				.collect(Collectors.groupingBy(i -> i % 2));
		if ((s.length() % 2 == 0 && cMap.keySet().size() == 1)
				|| (s.length() % 2 == 1 && cMap.get(1).size() == 1 && cMap.get(
						0).size() > 0))
			System.out.println("YES");
		else
			System.out.println("NO");

	}
}
