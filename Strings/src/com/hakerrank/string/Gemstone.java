package com.hakerrank.string;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Gemstone {

	public static void main(String[] args) {
		int[] bArr = new int[26];
		Scanner scanner = new Scanner(System.in);
		int l = Integer.parseInt(scanner.nextLine());
		int x = l;
		while (x-- > 0) {
			boolean[] aArr = new boolean[26];
			scanner.nextLine().chars().filter(a -> !aArr[a - 'a'])
					.forEach(a -> {
						aArr[a - 'a'] = true;
						bArr[a - 'a']++;
					});
		}
		scanner.close();
		System.out.println(IntStream.of(bArr).filter(a -> a == l).count());
	}

}
