package com.hakerrank.string;

import java.util.Scanner;

public class ISInputReader {
	
	public static String[] readInput(){
		Scanner sc = new Scanner(System.in);
		int l = Integer.parseInt(sc.nextLine());
		String[] sArr = new String[l];
		int i = 0;
		while (l-- > 0)
			sArr[i++] = sc.nextLine();
		sc.close();
		return sArr;
	}
	

}
