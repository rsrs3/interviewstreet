package com.hakerrank.string;

import java.util.Scanner;

public class MakeAnagram {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s1 = sc.nextLine();
		String s2 = sc.nextLine();
		int[] aArr = new int[26];
		int[] bArr = new int[26];
		s1.chars().forEach(a -> aArr[a - 'a']++);
		s2.chars().forEach(a -> bArr[a - 'a']++);
		sc.close();
		int count = 0;
		for (int i = 0; i < 26; ++i)
			if ((aArr[i] != 0 || bArr[i] != 0) && aArr[i]!=bArr[i])
				count += Math.abs(aArr[i] - bArr[i]);
		System.out.println(count);
	}

}
