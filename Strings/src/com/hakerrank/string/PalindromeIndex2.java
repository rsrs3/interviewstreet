package com.hakerrank.string;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PalindromeIndex2 {

	public static int COUNT = 26;

	public static void main(String[] args) {
		String[] sArr = readInput();
		for (String s : sArr)
			printPalindromeIndex(s);
	}

	private static void printPalindromeIndex(String s) {
		int[] aArr = new int[COUNT];
		int n = s.length();
		HashMap<Integer, LinkedList<Integer>> iMap = new HashMap<>();
		IntStream.range(0, s.length()).forEach(i -> {
			int idx = s.charAt(i) - 'a';
			aArr[idx]++;
			LinkedList<Integer> lList;
			if (iMap.containsKey(idx))
				lList = iMap.get(idx);
			else
				lList = new LinkedList<>();
			lList.add(i);
			iMap.put(idx, lList);
		});

		if (confirmIndex(s, -1)) {
			System.out.println(-1);
		} else {
			for (int i = 0; i < COUNT; ++i) {
				if (aArr[i] > 0) {
					aArr[i]--;
					if (checkPalindrome(aArr, n - 1)) {
						LinkedList<Integer> lList = iMap.get(i);
						for (int x : lList) {
							if (confirmIndex(s, x)) {
								System.out.println(x);
								return;
							}
						}
					}
					aArr[i]++;
				}
			}
			System.out.println(-1);
		}
	}

	private static boolean confirmIndex(String s, int i) {
		StringBuilder sb = new StringBuilder();
		if (i != -1) {
			sb.append(s.substring(0, i));
			sb.append(s.substring(i + 1, s.length()));
		} else {
			sb.append(s);
		}
		if (sb.toString().equals(sb.reverse().toString()))
			return true;
		else
			return false;
	}

	private static boolean checkPalindrome(int[] aArr, int l) {
		Map<Integer, List<Integer>> cMap = IntStream.of(aArr)
				.filter(i -> i > 0).mapToObj(i -> new Integer(i))
				.collect(Collectors.groupingBy(i -> i % 2));
		if ((l % 2 == 0 && cMap.keySet().size() == 1)
				|| (l % 2 == 1 && cMap.get(1).size() == 1))
			return true;
		else
			return false;
	}

	public static String[] readInput() {
		Scanner sc = new Scanner(System.in);
		int l = Integer.parseInt(sc.nextLine());
		String[] sArr = new String[l];
		int i = 0;
		while (l-- > 0)
			sArr[i++] = sc.nextLine();
		sc.close();
		return sArr;
	}

}
