package com.hakerrank.string;

import java.util.Scanner;

public class Panagram {

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			String in = sc.nextLine();
			checkPanagram(in);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void checkPanagram(String s) {
		boolean[] cArr = new boolean[26];
		for (char c : s.toCharArray()) {
			if (c != ' ')
				cArr[Character.toLowerCase(c) - 'a'] = true;
		}

		int flag = 0;
		for (boolean b : cArr) {
			if (!b) {
				flag = 1;
				break;
			}
		}

		if (flag == 0)
			System.out.println("pangram");
		else
			System.out.println("not pangram");
	}

}
