package com.hakerrank.string;

import java.util.Scanner;

public class TwoString {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int l = Integer.parseInt(sc.nextLine());
		String[] sArr = new String[l];
		int i = 0;
		while (l-- > 0)
			sArr[i++] = sc.nextLine()+" "+ sc.nextLine();
		for(String s:sArr)
			substring(s);
		sc.close();
	}
	
	public static void substring(String s){
		int[] fArr = new int[26];
		int[] bArr=new int[26];
		String[] sArr=s.split(" ");
		sArr[0].trim().chars().forEach(a->fArr[a-'a']++);
		sArr[1].trim().chars().forEach(a->bArr[a-'a']++);
		int flag=0;
		for(int i=0;i<26;++i){
			if(fArr[i]!=0 && bArr[i]!=0){
				flag=1;
				break;
			}
		}
		if(flag==1)
			System.out.println("YES");
		else{
			System.out.println("NO");
		}
	}

}
